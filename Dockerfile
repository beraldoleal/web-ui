FROM node:14.18.2-alpine as BUILD
WORKDIR /app
COPY package*.json ./
RUN npm install -g npm && npm install
COPY . .
ENV I4C_API_SERVER=$I4C_API_SERVER
RUN npm run build

FROM nginxinc/nginx-unprivileged:latest
COPY --from=BUILD /app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
