# Insights4CI - Client Web UI

This directory is holding a VueJS (v3) project, intended to provide a dashboard
to analyze data from CI's pipelines as part of the Insights4CI project.

## Production deployment

When deploying to production, please use `Dockerfile` that is prepared to build
this app optimized for "production". Also, it will use Nginx as web server.

Make sure you have an [API Server](https://gitlab.com/insights4ci/api-server)
up and running exposed to the world.

You can deploy it in your OCP or K8S cluster with the following command:

```
$ oc new-app https://gitlab.com/insights4ci/web-ui \
             --name=i4c-web-ui \
             --strategy=docker \
             --build-env I4C_API_SERVER="http://your-public-route-to-the-api-server"
```

Set the `I4C_API_SERVER` build environment variable to your API server address.

Now, expose this service to the world, so you can point it to your web browser:

```
$ oc expose svc/i4c-web-ui
route.route.openshift.io/i4c-web-ui exposed
```

You can now, use `oc status` to see the status of your deployment and the
public address of this service. After the build is completed your pod will be
accepting requests.

## Development deployment

If you would like to contribute with this project, it is better to have it
running locally with debug mode, development requirements installed and "watch
feature" enabled.

Visit [HACKING.md](HACKING.md) and [CONTRIBUTING.md](CONTRIBUTING.md) files for
details.
