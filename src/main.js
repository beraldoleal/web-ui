import { createApp } from 'vue'
import { Quasar } from 'quasar'

import App from './App.vue'
import router from './router'
import mitt from 'mitt'
import quasarUserOptions from './quasar-user-options'

const app = createApp(App)

app.config.globalProperties.emitter = mitt()
console.log("FOO")
console.log(process.env.NODE_ENV)
console.log(process.env.VUE_APP_I4C_API_SERVER)
app.config.globalProperties.api_server = process.env.VUE_APP_I4C_API_SERVER || "http://localhost:8000"
app.use(Quasar, quasarUserOptions).use(router).mount('#app')
